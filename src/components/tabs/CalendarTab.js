import React, { Component } from 'react'
import { Container, Text, Header, Content, Icon } from 'native-base'

class CalendarTab extends Component {
  static navigationOptions = {
    tabBarIcon: ({ tinColor }) => {
      return <Icon name='calendar' style={{ color: tinColor }}/>
    }
  }
  render() {
    return (
      <Container>
        <Header />
        <Content>
          <Text> This is the CalendarTab view</Text>
        </Content>
      </Container>
    )
  }
}

export default CalendarTab
