import React, { Component } from 'react'
import { Container, Text, Header, Content, Icon } from 'native-base'

class ExpensesTab extends Component {
  static navigationOptions = {
    tabBarIcon: ({ tinColor }) => {
      return <Icon name='cash' style={{ color: tinColor }} />
    }
  }
  render() {
    return (
      <Container>
        <Header />
        <Content>
          <Text> This is the ExpensesTab view</Text>
        </Content>
      </Container>
    )
  }
}

export default ExpensesTab
