import React, { Component } from 'react'
import { Container, Text, Header, Content, Icon } from 'native-base'

class PaymentsTab extends Component {
  static navigationOptions = {
    tabBarIcon: ({ tinColor }) => {
      return <Icon name='ios-card' style={{ color: tinColor }} />
    }
  }
  render() {
    return (
      <Container>
        <Header />
        <Content>
          <Text> This is the PaymentsTab view</Text>
        </Content>
      </Container>
    )
  }
}

export default PaymentsTab
