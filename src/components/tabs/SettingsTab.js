import React, { Component } from 'react'
import { Container, Text, Header, Content, Icon } from 'native-base'

class SettingsTab extends Component {
  static navigationOptions = {
    tabBarIcon: ({ tinColor }) => {
      return <Icon name='ios-settings' style={{ color: tinColor }} />
    }
  }
  render() {
    return (
      <Container>
        <Header />
        <Content>
          <Text> This is the SettingsTab view</Text>
        </Content>
      </Container>
    )
  }
}

export default SettingsTab
