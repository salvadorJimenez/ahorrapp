import { createBottomTabNavigator, createAppContainer } from 'react-navigation'
import { ExpensesTab, CalendarTab, PaymentsTab, SettingsTab } from '../components/tabs'

const tabNavigator = createBottomTabNavigator({
  ExpensesTab: {
    screen: ExpensesTab
  },
  CalendarTab: {
    screen: CalendarTab
  },
  PaymentsTab: {
    screen: PaymentsTab
  },
  SettingsTab: {
    screen: SettingsTab
  },
},{
  animationEnabled: true,
  swipeEnabled: true,
  tabBarPosition: 'bottom',
  tabBarOptions: {
    showIcon: true,
    showLabel: false,
    style: {
      backgroundColor: 'white',
    },
    activeTintColor: 'blue',
    inactiveTintColor: 'gray',
  }
})

export default createAppContainer(tabNavigator);
