import React, { Component } from 'react'
import TabNavigatorContainer from './src/routes/TabNavigatorContainer'

export default class App extends Component {
  render() {
    return (
      <TabNavigatorContainer />
    )
  }
}
